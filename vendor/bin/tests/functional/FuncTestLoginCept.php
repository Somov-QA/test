<?php
$I = new FunctionalTester($scenario);
$I->wantTo('perform actions and see result');
$I->amOnUrl("http://localhost/");
$I->amOnPage('test/login/');
$I->submitForm('form', ['username'=>'test', 'password'=>'123']);
$I->seeElement('h2');
$I->see("Correct", 'h2');
